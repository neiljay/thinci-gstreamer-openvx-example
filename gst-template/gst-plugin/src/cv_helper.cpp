#include <VX/vx.h>
#include <opencv2/opencv.hpp>
#include "cv_helper.h"

void cv_draw_points(vx_array points, vx_size numItems, unsigned char *buffer, int width, int height, vx_bool greyscale)
{
  // Create openCV matrix from incoming video buffer
  cv::Mat color_frame = cv::Mat (height, width, greyscale ? CV_8UC1 : CV_8UC3, buffer);
  vx_map_id map_id;
  vx_size stride = 0;
  vx_keypoint_t* object_positions_ptr = NULL;
  cv::Scalar cvWhite (255.0, 255.0, 255.0);
  cv::Scalar cvGreen (0.0, 255.0, 0,0);

  // Map the array for user access
  vxMapArrayRange (points, 0, numItems, &map_id, &stride, (void**)&object_positions_ptr, VX_READ_ONLY, VX_MEMORY_TYPE_HOST, 0);
  vx_keypoint_t* tmp_position_ptr = object_positions_ptr;
  
  // Iterate the array drawing a circle at each point
  for (unsigned i = 0; i < numItems; i++) {
    cv::Point cvp;
    vx_keypoint_t object_position = *tmp_position_ptr;
    tmp_position_ptr = (vx_keypoint_t*)((char*)tmp_position_ptr + stride);
    // Convert OVX to CV circle
    cvp.x = object_position.x;
    cvp.y = object_position.y;

    // Draw circle on input color_frame which is created from GStreamer mapped buffer data
    cv::circle (color_frame, cvp, 5, greyscale ? cvWhite : cvGreen);
  }

  //Finally unmap the array
  vxUnmapArrayRange (points, map_id);
}
