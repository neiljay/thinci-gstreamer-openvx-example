#ifndef __OCV_HELPER_H__
#define __OCV_HELPER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <VX/vx.h>

void cv_draw_points(vx_array points, vx_size num_items, unsigned char *buffer, int h, int w, vx_bool grey);

#ifdef __cplusplus
}
#endif

#endif
